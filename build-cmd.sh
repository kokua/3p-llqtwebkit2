#!/bin/bash

# make errors fatal
set -e

# So we can turn off building qt.
BUILD_QT=1
while getopts "x" OPTION
do
    case $OPTION in
    x)
        # '-c ReleaseNoQt' autobuild option makes use of this
        echo 'not building qt'
        BUILD_QT=0 
    ;;
    esac
done

QT_SOURCE_DIR="qt-everywhere-opensource-src"
LLQT_TEST_SUBDIRS_COMMON="qttestapp ssltest textmode"
LLQT_TEST_SUBDIRS_WINDOWS="$LLQT_TEST_SUBDIRS_COMMON 3dgl testgl"
LLQT_TEST_SUBDIRS_MAC="$LLQT_TEST_SUBDIRS_COMMON testgl"
LLQT_TEST_SUBDIRS_LINUX="$LLQT_TEST_SUBDIRS_COMMON"

if [ -z "$AUTOBUILD" ] ; then 
    fail
fi

if [ "$OSTYPE" = "cygwin" ] ; then
    export AUTOBUILD="$(cygpath -u $AUTOBUILD)"
fi

# load autbuild provided shell functions and variables
eval "$("$AUTOBUILD" source_environment)"

# turn on verbose debugging output for logging.
set -x

stage="$(pwd)"
packages="$stage/packages"
install="$stage"

ZLIB_INCLUDE="${packages}"/include/zlib
OPENSSL_INCLUDE="${packages}"/include/openssl
PNG_INCLUDE="${packages}"/include/libpng16

[ -f "$ZLIB_INCLUDE"/zlib.h ] || fail "You haven't installed the zlib package yet."
[ -f "$OPENSSL_INCLUDE"/ssl.h ] || fail "You haven't installed the openssl package yet."
[ -f "$PNG_INCLUDE"/png.h ] || fail "You haven't installed the libpng package yet."

# Restore all .sos
restore_sos ()
{
    for solib in "${stage}"/packages/lib/{debug,release}/lib{z,crypto,ssl}.so*.disable; do 
        if [ -f "$solib" ]; then
            mv -f "$solib" "${solib%.disable}"
        fi
    done
}


# Restore all .dylibs
restore_dylibs ()
{
    for dylib in "$stage/packages/lib"/{debug,release}/*.dylib.disable; do
        if [ -f "$dylib" ]; then
            mv "$dylib" "${dylib%.disable}"
        fi
    done
}


# darwin_fix_rpath [dylib|plugin] <qtlib_dir> <dylib_dir> <lib1> <lib2> <lib3> ...
#
# The dylibs built by qmake all have very concrete rpaths/install_names
# to libraries which is incorrect for our software.  The qmake system
# itself doesn't have adequate means to override these during the build
# so we have to do it ex post facto.  We do that here relying on the
# relative harmlessness of changing a path that doesn't exist.
#
# Example:
#
# darwin_fix_rpath plugin /build/stage/lib/release /build/stage/plugins/codecs liba.dylib libb.dylib
#
darwin_fix_rpaths ()
{
    [ $# -gt 3 ] || fail "darwin_fix_rpaths needs four or more args"

    local dylib_type="$1"; shift
    local qtlib_dir="$1"; shift
    local dylib_dir="$1"; shift
    for target; do
        if [ "$dylib_type" == dylib ]; then
            # plugins can keep their install_name
            local target_short="${target/4.*.*.dylib/4.dylib}"
            install_name_tool -id '@executable_path/../Resources/'"$target_short" "$dylib_dir"/"$target"
        fi
        for lib in libQtCore.4.dylib \
            libQtDeclarative.4.dylib \
            libQtGui.4.dylib \
            libQtMultimedia.4.dylib \
            libQtNetwork.4.dylib \
            libQtOpenGL.4.dylib \
            libQtScript.4.dylib \
            libQtScriptTools.4.dylib \
            libQtSql.4.dylib \
            libQtSvg.4.dylib \
            libQtTest.4.dylib \
            libQtWebKit.4.dylib \
            libQtXml.4.dylib; do

            install_name_tool -change "$qtlib_dir"/"$lib" '@executable_path/../Resources/'"$lib" "$dylib_dir"/"$target"
        done
    done
}

cd "$(dirname "$0")"

case "$AUTOBUILD_PLATFORM" in

    "windows")        
        load_vsvars
        
        export QMAKESPEC="win32-msvc2010"

        # build qt
        if [ $BUILD_QT -ne 0 ]
        then
            pushd "$stage"            
                QTDIR="$(pwd)/../$QT_SOURCE_DIR"
                export PATH="$QTDIR"/bin:"$PATH" 
        
                chmod +x "$QTDIR/configure.exe"
                common_configure_options="-opensource -confirm-license -platform $QMAKESPEC -fast \
                    -no-qt3support -no-phonon -no-phonon-backend \
                    -qt-libjpeg -system-zlib -system-libpng -openssl-linked -no-plugin-manifests -nomake demos -nomake examples \
                    -I "$(cygpath -m "$packages/include")" \
                    -I "$(cygpath -m "$ZLIB_INCLUDE")" \
                    -I "$(cygpath -m "$PNG_INCLUDE")""
        
                "$QTDIR/configure.exe" $common_configure_options -debug  -L "$(cygpath -m "$packages/lib/debug")"
                nmake /P
                
                "$QTDIR/configure.exe" $common_configure_options -release  -L "$(cygpath -m "$packages/lib/release")"
                nmake
            popd
    
            # Move around libraries to match autobuild layout.
            qtwebkit_libs_debug="QtCored4.dll QtCored4.lib QtGuid4.dll QtGuid4.lib \
                qtmaind.lib QtNetworkd4.dll QtNetworkd4.lib QtOpenGLd4.dll QtOpenGLd4.lib \
                QtWebKitd4.dll QtWebKitd4.lib QtXmlPatternsd4.dll"
            mkdir -p "$install/lib/debug"
            for lib in $qtwebkit_libs_debug ; do
                cp "$stage/lib/$lib" "$install/lib/debug"
            done
            
            qtwebkit_libs_release="QtCore4.dll QtCore4.lib QtGui4.dll QtGui4.lib \
                qtmain.lib QtNetwork4.dll QtNetwork4.lib QtOpenGL4.dll QtOpenGL4.lib \
                QtWebKit4.dll QtWebKit4.lib QtXmlPatterns4.dll"
            mkdir -p "$install/lib/release"
            for lib in $qtwebkit_libs_release ; do
                cp "$stage/lib/$lib" "$install/lib/release"
            done
            
            qtwebkit_imageplugins_debug="qgifd4.dll qicod4.dll qjpegd4.dll \
                qmngd4.dll qsvgd4.dll qtiffd4.dll"
            mkdir -p "$install/lib/debug/imageformats"
            for plugin in $qtwebkit_imageplugins_debug ; do
                cp "$stage/plugins/imageformats/$plugin" "$install/lib/debug/imageformats"
            done            

            qtwebkit_imageplugins_release="qgif4.dll qico4.dll qjpeg4.dll \
                qmng4.dll qsvg4.dll qtiff4.dll"
            mkdir -p "$install/lib/release/imageformats"
            for plugin in $qtwebkit_imageplugins_release ; do
                cp "$stage/plugins/imageformats/$plugin" "$install/lib/release/imageformats"
            done

            qtwebkit_codecs_debug="qjpcodecsd4.dll qcncodecsd4.dll qkrcodecsd4.dll qtwcodecsd4.dll"
            mkdir -p "$install/lib/debug/codecs"
            for codec in $qtwebkit_codecs_debug ; do
                cp "$stage/plugins/codecs/$codec" "$install/lib/debug/codecs"
            done
            
            qtwebkit_codecs_release="qcncodecs4.dll qjpcodecs4.dll qkrcodecs4.dll qtwcodecs4.dll"
            mkdir -p "$install/lib/release/codecs"
            for codec in $qtwebkit_codecs_release ; do
                cp "$stage/plugins/codecs/$codec" "$install/lib/release/codecs"
            done
        fi
                
        # Now build llqtwebkit...
        pushd llqtwebkit
            export PATH="$install/bin/":"$stage/../$QT_SOURCE_DIR/bin":"$PATH"
        
            qmake -spec $QMAKESPEC CONFIG-=debug
            nmake clean
            nmake
        
            qmake -spec $QMAKESPEC CONFIG+=debug
            nmake clean
            nmake

            mkdir -p "$install/lib/debug"
            cp "debug/llqtwebkitd.lib"  "$install/lib/debug"        

            mkdir -p "$install/lib/release"
            cp "release/llqtwebkit.lib" "$install/lib/release"
        
            mkdir -p "$install/include"
            cp "llqtwebkit.h" "$install/include"
        
             conditionally build unit tests
             if [ "${DISABLE_UNIT_TESTS:-0}" = "0" ]; then
                for test in $LLQT_TEST_SUBDIRS_WINDOWS; do
                    pushd tests/"$test"
                        qmake -spec $QMAKESPEC CONFIG-=debug 
                        nmake clean
                        nmake
                   popd
                done
            fi
        popd

        # Helper script for maintainers
        cp -a win32/3p-qt-vars.bat $stage/bin
    ;;

    "darwin")
        # Force libz static linkage by moving .dylibs out of the way
        trap restore_dylibs EXIT
        for dylib in "$stage"/packages/lib/{debug,release}/libz*.dylib; do
            if [ -f "$dylib" ]; then
                mv "$dylib" "$dylib".disable
            fi
        done

        # Select SDK with full path, configure's -sdk option doesn't
        # understand the short form used by xcodebuild.
        #
        # sdk=/Developer/SDKs/MacOSX10.6.sdk/
        # sdk=/Developer/SDKs/MacOSX10.7.sdk/
        # sdk=/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.7.sdk/
        sdk=/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.6.sdk/
        sdk_short=macosx10.6

        # Mac deployment targets are generally set in the qmake mkspec plumbing
        # and we don't always successfully override/promote it.  You'll probably
        # see a lot of 10.4/10.5 in the builds without more aggressive action.

        platform=macx-llvm

        QTDIR="$(pwd)/$QT_SOURCE_DIR"

        # Build qt...
        if [ $BUILD_QT -ne 0 ]
        then
            pushd "$QT_SOURCE_DIR"
                echo "yes" | \
                    OPENSSL_LIBS="-L$packages/lib/release -lssl -lcrypto" \
                    ./configure -opensource -shared -release -platform $platform -arch i386 -cocoa -fast \
                        -no-qt3support -no-framework -no-xmlpatterns -no-phonon -webkit -sdk "$sdk" -dwarf2 \
                        -webkit -svg -openssl-linked -system-zlib -system-libpng -qt-libtiff -qt-libmng -qt-libjpeg \
                        -I"$packages/include" -I"$ZLIB_INCLUDE" -I"$PNG_INCLUDE" \
                        -L"$packages/lib/release" \
                        --prefix="$install" --libdir="$install/lib/release" \
                        -nomake examples -nomake demos -nomake docs -nomake translations -nomake tools
                PATH="$QTDIR/bin":"$PATH" make -j4
                PATH="$QTDIR/bin":"$PATH" make install

                # make -j4 -C "src/3rdparty/webkit/JavaScriptCore"
                # cp -a "src/3rdparty/webkit/JavaScriptCore/release/libjscore.a" "$install/lib/release"

                # Rewrite the install_name and rpaths of all .dylibs built, including plugins
                darwin_fix_rpaths dylib "$install"/lib/release "$install"/lib/release \
                    libQtCore.4.7.1.dylib \
                    libQtDeclarative.4.7.1.dylib \
                    libQtGui.4.7.1.dylib \
                    libQtMultimedia.4.7.1.dylib \
                    libQtNetwork.4.7.1.dylib \
                    libQtOpenGL.4.7.1.dylib \
                    libQtScript.4.7.1.dylib \
                    libQtScriptTools.4.7.1.dylib \
                    libQtSql.4.7.1.dylib \
                    libQtSvg.4.7.1.dylib \
                    libQtTest.4.7.1.dylib \
                    libQtWebKit.4.7.1.dylib \
                    libQtXml.4.7.1.dylib

                darwin_fix_rpaths plugin "$install"/lib/release "$install"/plugins/codecs \
                    libqcncodecs.dylib \
                    libqjpcodecs.dylib \
                    libqkrcodecs.dylib \
                    libqtwcodecs.dylib

                darwin_fix_rpaths plugin "$install"/lib/release "$install"/plugins/imageformats \
                    libqgif.dylib \
                    libqico.dylib \
                    libqjpeg.dylib \
                    libqmng.dylib \
                    libqsvg.dylib \
                    libqtiff.dylib
            popd
        fi

        # Now build llqtwebkit
        pushd llqtwebkit
            if [ true ]; then
                # qmake-based build of static library

                PATH="$install/bin/":"$PATH" qmake -platform $platform CONFIG-=debug \
                    CONFIG-=debug QMAKE_MAC_SDK="$sdk" QMAKE_MACOSX_DEPLOYMENT_TARGET=10.6
                PATH="$install/bin/":"$PATH" make
                # PATH="$install/bin/":"$PATH" make install

                # Would like preceding to work but doesn't yet so we still do this:
                mkdir -p "$install"/lib/release
                cp -a libllqtwebkit.a "$install"/lib/release

                mkdir -p "$install"/include
                cp -a llqtwebkit.h "$install"/include
            else
                # Legacy build with xcodeproj description and fake QTDir

                if [ ! -e QTDIR ]
                then
                    ln -s "$install" QTDIR
                    # ln -s "$QT_SOURCE_DIR" QTDIR
                    # ln -s "$QTDIR" QTDIR
                fi
                xcodebuild -project llqtwebkit.xcodeproj -sdk $sdk_short -arch i386 -target llqtwebkit -configuration Release

                mkdir -p "$install/lib/release"
                cp -a "build/Release/libllqtwebkit.dylib" "$install/lib/release"

                mkdir -p "$install/include"
                cp -a "llqtwebkit.h" "$install/include"
            fi

            # conditionally build unit tests
            # Tests are built but not run (see README.Linden for details).
            if [ "${DISABLE_UNIT_TESTS:-0}" = "0" ]; then
                # Prepare Resources peer directory of support dylibs
                mkdir -p tests/Resources
                ln -sf "$packages"/lib/release/*.dylib tests/Resources/
                ln -sf "$install"/lib/release/*.dylib tests/Resources/

                for test in $LLQT_TEST_SUBDIRS_MAC; do
                    pushd tests/"$test"
                    PATH="$install/bin/":"$PATH" qmake -platform $platform \
                        CONFIG-=debug QMAKE_MAC_SDK="$sdk" QMAKE_MACOSX_DEPLOYMENT_TARGET=10.6
                    PATH="$install/bin/":"$PATH" make
                    if [ "$test" == "qttestapp" ]; then
                        # Needs/wants qt_menu.nib
                        cp -a "$QTDIR"/src/gui/mac/qt_menu.nib .
                    fi
                    popd
                done
            fi
        popd
    ;;

    "linux")
        # Linux build environment at Linden comes pre-polluted with stuff that can
        # seriously damage 3rd-party builds.  Environmental garbage you can expect
        # includes:
        #
        #    DISTCC_POTENTIAL_HOSTS     arch           root        CXXFLAGS
        #    DISTCC_LOCATION            top            branch      CC
        #    DISTCC_HOSTS               build_name     suffix      CXX
        #    LSDISTCC_ARGS              repo           prefix      CFLAGS
        #    cxx_version                AUTOBUILD      SIGN        CPPFLAGS
        #
        # So, clear out bits that shouldn't affect our configure-directed build
        # but which do nonetheless.
        #
        unset DISTCC_HOSTS CC CXX CFLAGS CPPFLAGS CXXFLAGS

        # The 'configure' script does the wrong thing with qmake/Makefile 
        # generation and all the qmake.conf configuration files and if you
        # don't, for example, set CC directly, it will construct a CC by
        # concatenating every QMAKE_CC definition it can find regardless of
        # whether one overwrites another.  It's stupid.  Really.  So keep
        # these 'bootstrap_*' settings in agreement with $platform.
        bootstrap_cc=gcc
        bootstrap_cxx=g++

        platform=linux-g++-32-ll
        buildkey=LL"$(date +%s)"
        makeflags="-j2"
        build_fake_debug=1                      # If true, just use a copy of release

        # Prefer gcc-4.6 if available.
        if [[ -x /usr/bin/gcc-4.6 && -x /usr/bin/g++-4.6 ]]; then
            platform=linux-g++46-32-ll
            bootstrap_cc=gcc-4.6
            bootstrap_cxx=g++-4.6
        fi
        
        # Handle any deliberate platform targeting (this will likely
        # be ineffective with mkspecs).
        if [ -z "$TARGET_CPPFLAGS" ]; then
            # Remove sysroot contamination from build environment
            unset CPPFLAGS
        else
            # Incorporate special pre-processing flags
            export CPPFLAGS="$TARGET_CPPFLAGS" 
        fi
        
        # Force static linkage to libz, libcrypto, libssl by moving .sos out of the way
        trap restore_sos EXIT
        for solib in "${stage}"/packages/lib/{debug,release}/lib{z,crypto,ssl}.so*; do
            if [ -f "$solib" ]; then
                mv -f "$solib" "$solib".disable
            fi
        done

        # Debug first
        if [ ! $build_fake_debug ]; then
            # Debug linux build produces about 2GB of binary, we'll use a copy of release by default.
            LIB_DIR="$install/lib/debug"

            if [ $BUILD_QT -ne 0 ]
            then
                # Conditionally build Qt (target ReleaseNoQt skips the Qt build)

                pushd "$QT_SOURCE_DIR"
                    export QTDIR="$(pwd)"
                    echo "DISTCC_HOSTS=$DISTCC_HOSTS"
    
                    echo "yes" | \
                        OPENSSL_LIBS="-L$packages/lib/debug -lssl -lcrypto" \
                        QMAKESPEC="$platform" CC="$bootstrap_cc" CXX="$bootstrap_cxx" \
                        ./configure \
                        -v -platform $platform -shared -arch i386 -debug -fontconfig -fast -buildkey "$buildkey" \
                        -no-xmlpatterns -no-phonon -no-3dnow -no-sse -no-sse2 -no-sse3 -no-ssse3 -no-sse4.1 -no-sse4.2 \
                        -no-gtkstyle -no-xinput -no-sm -no-qt3support \
                        -no-sql-sqlite -no-scripttools -no-cups -no-dbus -no-glib -no-pch -no-xkb \
                        -opengl desktop -xrender -svg -webkit -opensource \
                        -openssl-linked -system-libpng -system-zlib -qt-libtiff -qt-libmng -qt-libjpeg \
                        -I"$packages/include" -I"$ZLIB_INCLUDE" -I"$PNG_INCLUDE" \
                        -L"$packages/lib/debug" -no-rpath \
                        --prefix="$install" --libdir="$install/lib/debug" \
                        -nomake examples -nomake demos -nomake docs -nomake translations -nomake tools
                    PATH="$QTDIR/bin:$PATH" make $makeflags
                    PATH="$QTDIR/bin:$PATH" make install
                    
                    # libjscore.a doesn't get installed but some libs depend on it.
                    # cp -a "./src/3rdparty/webkit/JavaScriptCore/debug/libjscore.a" "$LIB_DIR"
                popd
            fi

            # Now build llqtwebkit...
            pushd llqtwebkit
                PATH="$install/bin/":"$PATH" qmake -platform $platform CONFIG-=release # $QMAKE_VARS 
                PATH="$install/bin/":"$PATH" make $makeflags

                # Install bits
                cp -a "libllqtwebkit.a" "$LIB_DIR"
                mkdir -p "$install/include"
                cp -a "llqtwebkit.h" "$install/include"

                # conditionally build unit tests
                if [ "${DISABLE_UNIT_TESTS:-0}" = "0" ]; then
                    for test in $LLQT_TEST_SUBDIRS_LINUX; do
                        pushd tests/"$test"
                            PATH="$install/bin/":"$PATH" qmake -platform $platform CONFIG-=release # $QMAKE_VARS 
                            PATH="$install/bin/":"$PATH" make $makeflags
                        popd
                    done
                fi
            popd
        fi

        # Release
        LIB_DIR="$install/lib/release"

        if [ $BUILD_QT -ne 0 ]
        then
            # Conditionally build Qt (target ReleaseNoQt skips the Qt build)

            pushd "$QT_SOURCE_DIR"
                export QTDIR="$(pwd)"
                echo "DISTCC_HOSTS=$DISTCC_HOSTS"

                echo "yes" | \
                    OPENSSL_LIBS="-L$packages/lib/release -lssl -lcrypto" \
                    QMAKESPEC="$platform" CC="$bootstrap_cc" CXX="$bootstrap_cxx" \
                    ./configure \
                    -v -platform $platform -shared -arch i386 -release -fontconfig -fast -buildkey "$buildkey" \
                    -no-xmlpatterns -no-phonon -no-3dnow -no-sse -no-sse2 -no-sse3 -no-ssse3 -no-sse4.1 -no-sse4.2 \
                    -no-gtkstyle -no-xinput -no-sm -no-qt3support \
                    -no-sql-sqlite -no-scripttools -no-cups -no-dbus -no-glib -no-pch -no-xkb \
                    -opengl desktop -xrender -svg -webkit -opensource \
                    -openssl-linked -system-libpng -system-zlib -qt-libtiff -qt-libmng -qt-libjpeg \
                    -I"$packages/include" -I"$ZLIB_INCLUDE" -I"$PNG_INCLUDE" \
                    -L"$packages/lib/release" -no-rpath \
                    --prefix="$install" --libdir="$install/lib/release" \
                    -nomake examples -nomake demos -nomake docs -nomake translations -nomake tools
                PATH="$QTDIR/bin:$PATH" make $makeflags
                PATH="$QTDIR/bin:$PATH" make install

                # libjscore.a doesn't get installed but some libs depend on it.
                # cp -a "./src/3rdparty/webkit/JavaScriptCore/release/libjscore.a" "$install/lib/release"
            popd
        fi

        # Now build llqtwebkit...
        pushd llqtwebkit
            PATH="$install/bin/":"$PATH" qmake -platform $platform CONFIG-=debug # $QMAKE_VARS 
            PATH="$install/bin/":"$PATH" make $makeflags

            # Install bits
            cp -a "libllqtwebkit.a" "$LIB_DIR"
            mkdir -p "$install/include"
            cp -a "llqtwebkit.h" "$install/include"

            # conditionally build unit tests
            # Tests are built but not run (see README.Linden for details).
            if [ "${DISABLE_UNIT_TESTS:-0}" = "0" ]; then
                for test in $LLQT_TEST_SUBDIRS_LINUX; do
                    pushd tests/"$test"
                        PATH="$install/bin/":"$PATH" qmake -platform $platform CONFIG-=debug # $QMAKE_VARS 
                        PATH="$install/bin/":"$PATH" make $makeflags
                    popd
                done
            fi
        popd

        # Possible fake debug build
        if [ $build_fake_debug ]; then
            rm -rf "$LIB_DIR/../debug"
            cp -a "$LIB_DIR" "$LIB_DIR/../debug"
        fi
    ;;
    "linux64")
        # Linux build environment at Linden comes pre-polluted with stuff that can
        # seriously damage 3rd-party builds.  Environmental garbage you can expect
        # includes:
        #
        #    DISTCC_POTENTIAL_HOSTS     arch           root        CXXFLAGS
        #    DISTCC_LOCATION            top            branch      CC
        #    DISTCC_HOSTS               build_name     suffix      CXX
        #    LSDISTCC_ARGS              repo           prefix      CFLAGS
        #    cxx_version                AUTOBUILD      SIGN        CPPFLAGS
        #
        # So, clear out bits that shouldn't affect our configure-directed build
        # but which do nonetheless.
        #
        unset DISTCC_HOSTS CC CXX CFLAGS CPPFLAGS CXXFLAGS

        # The 'configure' script does the wrong thing with qmake/Makefile 
        # generation and all the qmake.conf configuration files and if you
        # don't, for example, set CC directly, it will construct a CC by
        # concatenating every QMAKE_CC definition it can find regardless of
        # whether one overwrites another.  It's stupid.  Really.  So keep
        # these 'bootstrap_*' settings in agreement with $platform.
        bootstrap_cc=gcc
        bootstrap_cxx=g++

        platform=linux-g++-64
        buildkey=LL"$(date +%s)"
        makeflags="-j2"
        build_fake_debug=1                      # If true, just use a copy of release

        # Prefer gcc-4.6 if available.
        if [[ -x /usr/bin/gcc-4.6 && -x /usr/bin/g++-4.6 ]]; then
            platform=linux-g++-64
            bootstrap_cc=gcc-4.6
            bootstrap_cxx=g++-4.6
        fi
        
        # Handle any deliberate platform targeting (this will likely
        # be ineffective with mkspecs).
        if [ -z "$TARGET_CPPFLAGS" ]; then
            # Remove sysroot contamination from build environment
            unset CPPFLAGS
        else
            # Incorporate special pre-processing flags
            export CPPFLAGS="$TARGET_CPPFLAGS" 
        fi
        
        # Force static linkage to libz, libcrypto, libssl by moving .sos out of the way
        trap restore_sos EXIT
        for solib in "${stage}"/packages/lib/{debug,release}/lib{z,crypto,ssl}.so*; do
            if [ -f "$solib" ]; then
                mv -f "$solib" "$solib".disable
            fi
        done

        # Debug first
        if [ ! $build_fake_debug ]; then
            # Debug linux build produces about 2GB of binary, we'll use a copy of release by default.
            LIB_DIR="$install/lib/debug"

            if [ $BUILD_QT -ne 0 ]
            then
                # Conditionally build Qt (target ReleaseNoQt skips the Qt build)

                pushd "$QT_SOURCE_DIR"
                    export QTDIR="$(pwd)"
                    echo "DISTCC_HOSTS=$DISTCC_HOSTS"
    
                    echo "yes" | \
                        OPENSSL_LIBS="-L$packages/lib/debug -lssl -lcrypto" \
                        QMAKESPEC="$platform" CC="$bootstrap_cc" CXX="$bootstrap_cxx" \
                        ./configure \
                        -v -platform $platform -shared -arch x86_64 -debug -fontconfig -fast -buildkey "$buildkey" \
                        -no-xmlpatterns -no-phonon -no-3dnow -no-sse -no-sse2 -no-sse3 -no-ssse3 -no-sse4.1 -no-sse4.2 \
                        -no-gtkstyle -no-xinput -no-sm -no-qt3support \
                        -no-sql-sqlite -no-scripttools -no-cups -no-dbus -no-glib -no-pch -no-xkb \
                        -opengl desktop -xrender -svg -webkit -opensource \
                        -openssl-linked -system-libpng -system-zlib -qt-libtiff -qt-libmng -qt-libjpeg \
                        -I"$packages/include" -I"$ZLIB_INCLUDE" -I"$PNG_INCLUDE" \
                        -L"$packages/lib/debug" -no-rpath \
                        --prefix="$install" --libdir="$install/lib/debug" \
                        -nomake examples -nomake demos -nomake docs -nomake translations -nomake tools
                    PATH="$QTDIR/bin:$PATH" make $makeflags
                    PATH="$QTDIR/bin:$PATH" make install
                    
                    # libjscore.a doesn't get installed but some libs depend on it.
                    # cp -a "./src/3rdparty/webkit/JavaScriptCore/debug/libjscore.a" "$LIB_DIR"
                popd
            fi

            # Now build llqtwebkit...
            pushd llqtwebkit
                PATH="$install/bin/":"$PATH" qmake -platform $platform CONFIG-=release # $QMAKE_VARS 
                PATH="$install/bin/":"$PATH" make $makeflags

                # Install bits
                cp -a "libllqtwebkit.a" "$LIB_DIR"
                mkdir -p "$install/include"
                cp -a "llqtwebkit.h" "$install/include"

                # conditionally build unit tests
                if [ "${DISABLE_UNIT_TESTS:-0}" = "1" ]; then
                    for test in $LLQT_TEST_SUBDIRS_LINUX; do
                        pushd tests/"$test"
                            PATH="$install/bin/":"$PATH" qmake -platform $platform CONFIG-=release # $QMAKE_VARS 
                            PATH="$install/bin/":"$PATH" make $makeflags
                        popd
                    done
                fi
            popd
        fi

        # Release
        LIB_DIR="$install/lib/release"

        if [ $BUILD_QT -ne 0 ]
        then
            # Conditionally build Qt (target ReleaseNoQt skips the Qt build)

            pushd "$QT_SOURCE_DIR"
                export QTDIR="$(pwd)"
                echo "DISTCC_HOSTS=$DISTCC_HOSTS"

                echo "yes" | \
                    OPENSSL_LIBS="-L$packages/lib/release -lssl -lcrypto" \
                    QMAKESPEC="$platform" CC="$bootstrap_cc" CXX="$bootstrap_cxx" \
                    ./configure \
                    -v -platform $platform -shared -arch x86_64 -release -fontconfig -fast -buildkey "$buildkey" \
                    -no-xmlpatterns -no-phonon -no-3dnow -no-sse -no-sse2 -no-sse3 -no-ssse3 -no-sse4.1 -no-sse4.2 \
                    -no-gtkstyle -no-xinput -no-sm -no-qt3support \
                    -no-sql-sqlite -no-scripttools -no-cups -no-dbus -no-glib -no-pch -no-xkb \
                    -opengl desktop -xrender -svg -webkit -opensource \
                    -openssl-linked -system-libpng -system-zlib -qt-libtiff -qt-libmng -qt-libjpeg \
                    -I"$packages/include" -I"$ZLIB_INCLUDE" -I"$PNG_INCLUDE" \
                    -L"$packages/lib/release" -no-rpath \
                    --prefix="$install" --libdir="$install/lib/release" \
                    -nomake examples -nomake demos -nomake docs -nomake translations -nomake tools
                PATH="$QTDIR/bin:$PATH" make $makeflags
                PATH="$QTDIR/bin:$PATH" make install

                # libjscore.a doesn't get installed but some libs depend on it.
                # cp -a "./src/3rdparty/webkit/JavaScriptCore/release/libjscore.a" "$install/lib/release"
            popd
        fi

        # Now build llqtwebkit...
        pushd llqtwebkit
            PATH="$install/bin/":"$PATH" qmake -platform $platform CONFIG-=debug # $QMAKE_VARS 
            PATH="$install/bin/":"$PATH" make $makeflags

            # Install bits
            cp -a "libllqtwebkit.a" "$LIB_DIR"
            mkdir -p "$install/include"
            cp -a "llqtwebkit.h" "$install/include"

            # conditionally build unit tests
            # Tests are built but not run (see README.Linden for details).
            if [ "${DISABLE_UNIT_TESTS:-0}" = "1" ]; then
                for test in $LLQT_TEST_SUBDIRS_LINUX; do
                    pushd tests/"$test"
                        PATH="$install/bin/":"$PATH" qmake -platform $platform CONFIG-=debug # $QMAKE_VARS 
                        PATH="$install/bin/":"$PATH" make $makeflags
                    popd
                done
            fi
        popd

        # Possible fake debug build
        if [ $build_fake_debug ]; then
            rm -rf "$LIB_DIR/../debug"
            cp -a "$LIB_DIR" "$LIB_DIR/../debug"
        fi
    ;;

esac

mkdir -p "$install"/LICENSES/
cp -a "LLQTWEBKIT_LICENSE.txt" "$install/LICENSES/llqtwebkit.txt"

mkdir -p "$install"/docs/llqtwebkit/
cp -a README.Linden "$install"/docs/llqtwebkit/

pass

