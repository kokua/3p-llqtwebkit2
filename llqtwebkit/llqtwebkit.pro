TEMPLATE = lib
mac: {
    # CONFIG += shared
    CONFIG += static staticlib
} else {
    CONFIG += static staticlib # we always build as static lib whether Qt is static or not
}
TARGET = 
DEPENDPATH += .
INCLUDEPATH += .

include(llqtwebkit.pri)

QT += webkit opengl network gui

win32:CONFIG(debug,debug|release) {
  TARGET = llqtwebkitd
}

win32:static {
  # If building a static library on Windows, use
  # compatibility-mode debug symbols to get debug
  # information in the library.
  QMAKE_CFLAGS_RELEASE += -Z7
  QMAKE_CFLAGS_RELEASE_WITH_DEBUGINFO += -Z7
  QMAKE_CFLAGS_DEBUG += -Z7

  QMAKE_CXXFLAGS_RELEASE += -Z7
  QMAKE_CXXFLAGS_RELEASE_WITH_DEBUGINFO += -Z7
  QMAKE_CXXFLAGS_DEBUG += -Z7
}

RCC_DIR     = $$PWD/.rcc
UI_DIR      = $$PWD/.ui
MOC_DIR     = $$PWD/.moc
OBJECTS_DIR = $$PWD/.obj
