TEMPLATE = app
TARGET = 
DEPENDPATH += .
INCLUDEPATH += .
INCLUDEPATH += ../../
CONFIG -= app_bundle
CONFIG += console

QT += webkit network

unix:!mac {
    DEFINES += LL_LINUX
    LIBS += $$PWD/../../libllqtwebkit.a
}

mac {
    DEFINES += LL_OSX
    LIBS += -L"$$PWD/../../../stage/lib/release/" -lllqtwebkit
}

win32 {
    DEFINES += _WINDOWS
    INCLUDEPATH += ../
    DESTDIR=../build
    LIBS += user32.lib 
    release {
      LIBS += $$PWD/../../Release/llqtwebkit.lib
    }
}

include(../../static.pri)

SOURCES += textmode.cpp
