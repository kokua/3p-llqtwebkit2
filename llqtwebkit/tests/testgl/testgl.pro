TEMPLATE = app
TARGET = 
DEPENDPATH += .
INCLUDEPATH += .
INCLUDEPATH += ../../
CONFIG -= app_bundle

QT += webkit opengl network

unix:!mac {
    DEFINES += LL_LINUX
    LIBS += -lglui -lglut
    LIBS += $$PWD/../../libllqtwebkit.a
}

mac {
    DEFINES += LL_OSX
    LIBS += -framework GLUT -framework OpenGL
    LIBS += -L"$$PWD/../../../stage/lib/release/" -lllqtwebkit
}

win32 {
    DEFINES += _WINDOWS
    INCLUDEPATH += ../
    INCLUDEPATH += $$PWD/../../../stage/packages/include
    DESTDIR=../build
    release {
        LIBS += $$PWD/../../Release/llqtwebkit.lib
        LIBS += $$PWD/../../../stage/packages/lib/release/freeglut_static.lib
        LIBS += comdlg32.lib
    }
}

include(../../static.pri)

SOURCES += testgl.cpp
