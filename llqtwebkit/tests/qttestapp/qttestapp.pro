TEMPLATE = app
TARGET = 
DEPENDPATH += .
INCLUDEPATH += .
INCLUDEPATH += ../../
CONFIG -= app_bundle

include(../../static.pri)

QT += webkit opengl network

# QT -= core gui

unix:!mac {
    DEFINES += LL_LINUX
    LIBS += $$PWD/../../libllqtwebkit.a
}

mac {
    DEFINES += LL_OSX
    LIBS += -L"$$PWD/../../../stage/lib/release/" -lllqtwebkit
}


win32{
    DEFINES += _WINDOWS
    INCLUDEPATH += ../
    DESTDIR=../build
    release {
      LIBS += $$PWD/../../Release/llqtwebkit.lib
    }
}

# Input
SOURCES += main.cpp
FORMS += window.ui
